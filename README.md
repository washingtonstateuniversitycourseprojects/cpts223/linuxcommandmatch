# LinuxCommandMatch

This project is a game in which users match linux commands to definitions. It uses templates to store the commands in nodes to be displayed in a random order for users to match to definitions, and user information is stored in a file for reuse.