/************************************************************************
* Programmer: Emma Mickas                                               *
* CptS 223, Spring 2020                                                 *
* Programming Assignment 1                                              *
* January 30, 2020                                                      *
* Decription: This file contains function definitions to play match.    *
************************************************************************/

#include "List.h"
#include "profiles.h"



/************************************************************************
* Programming Assignment 1                                              *
* Display Rules:                                                        *
*                                                                       *
* Description: Displays the rules of the game for the player            *
************************************************************************/

void displayRules()
{
    cout << "This is a game of match!" << endl;
    cout << "You will be given a number of linux commands etween 5 and 30 of your selection." << endl;
    cout << "You will then be presented with each of these commands one at a time, along with three descriptions." << endl;
    cout << "Match the correct description of the given command to win a point!" << endl;
    cout << "But be careful, if you get a match wrong, you'll lose a point!" << endl;
    cout << "Good luck!\n" << endl;
}



/************************************************************************
* Programming Assignment 1                                              *
* Enter New Name:                                                       *
*                                                                       *
* Description: Allows the user to enter a name into the profile array   *
************************************************************************/

int enterNewName(Profile profiles[])
{
    string newName;

    int i = 0, j = 1, w = 0;

    cout << "Enter your name: ";

    do
    {
        getline(cin, newName);

    } while (newName == "");

    if (profiles[i].name == "") //If profile array is empty
    {
        profiles[i].name = newName;
    }
    else
    {
        while (profiles[j].name != "") //Find end of existing profiles
        {
            i++;

            j++;
        }

        for (i; i >= 0; i--, j--) //Loop until front of list
        {
            profiles[j].name = profiles[i].name; //Shift name forward one place in profile array
        }

        i = 0; //Return to first position of list

        profiles[i].name = newName;

        profiles[i].score = 0;
    }

    return i; //Return place of current profile
}



/************************************************************************
* Programming Assignment 1                                              *
* Number of Commands:                                                   *
*                                                                       *
* Description: Finds and returns the number of commands in the commands *
*              linked list.                                             *
************************************************************************/

int numCommands(List<string, string> commandList)
{
    int numCommands = 0;

    ListNode<string, string>* current = commandList.getHead();

    while (current) //Loops until empty node
    {
        numCommands++; //Increment to record number of commands

        current->setNext(current->getNext()); //Increment through linked list
    }
    return numCommands; //Return total commands in linked list
}



/************************************************************************
* Programming Assignment 1                                              *
* Shuffle Descriptions:                                                 *
*                                                                       *
* Description: Shuffles the descriptions to display in a random order   *
*              for user to match to command.                            *
************************************************************************/

int shuffleDescriptions(int correctDescription, int descriptionOptions[3], int numCommands)
{
    int duplicate = 0;

    int correctPlace;

    for (int i = 0; i < 3; i++) //Loop through description option array
    {
        descriptionOptions[i] = -1; //Initialize all values in array to -1 to avoid duplicates
    }

    correctPlace = rand() % 3; //Randomly place correct description

    descriptionOptions[correctPlace] = correctDescription; //Set description number to correct place in description option array

    for (int i = 0; i < 3; i++) //Loops through description option list
    {
        do
        {
            if (i == correctPlace) //Correct place has already been set with correct description
            {
                i++; //Skip correct place
            }

            if (i > 2) //If incrementation has caused i to move out of the list
            {
                break;
            }

            descriptionOptions[i] = rand() % numCommands; //Sets random number description

            duplicate = 0; //Sets duplicate to not duplicate for new number

            for (int j = 0; j < 3; j++) //Loops through current list
            {
                if (i == j) //Skips same place check
                {
                    j++;
                }

                if (j > 2) //If incrementation has caused j to move out of the list
                {
                    break;
                }

                if (descriptionOptions[j] == descriptionOptions[i]) //If number already exists
                {
                    duplicate = 1; //Set duplicate to dupliate

                    break; //Break to reset number
                }

            }

        } while (duplicate == 1); //Loops until number is unique
    }

    return correctPlace; //Return correct description spot
}



/************************************************************************
* Programming Assignment 1                                              *
* Shuffle Order:                                                        *
*                                                                       *
* Description: Randomizes order of commands to play matching game.      *
************************************************************************/

void shuffleOrder(int commandsUsed[100], int numCommands)
{
    int duplicate = 0;

    for (int i = 0; i < 100; i++) //Loops through command number list
    {
        commandsUsed[i] = -1; //Initialize all commands as -1 to avoid duplicates
    }
    for (int i = 0; i < numCommands; i++) //Loops through command number list
    {
        do
        {
            commandsUsed[i] = rand() % numCommands; //Sets random number command

            duplicate = 0; //Sets duplicate to not duplicate

            for (int j = 0; j < i; j++) //Loops through current list
            {
                if (commandsUsed[j] == commandsUsed[i]) //If number already exists
                {
                    duplicate = 1; //Set duplicate to dupliate

                    break; //Break to reset number
                }
            }

        } while (duplicate == 1); //Loops until number is unique
    }
}



/************************************************************************
* Programming Assignment 1                                              *
* Play Round:                                                           *
*                                                                       *
* Description: Executes a single round of the game at a time.           *
************************************************************************/

void playRound(List<string, string> &commandList, Profile &player, int roundNumber, int commandsUsed[30], int numCommands)
{
    string throwaway;

    ListNode<string, string>* current = commandList.getHead(); //Initialize node used to traverse linked list of commands

    int descriptionOptions[3]; //Initialize array containing possible description matches

    int correctPlace = 0, guess = 0; //Initialize place of correct description and user guess

    cout << "Round " << roundNumber << "!" << endl; //Message to user

    cout << "Match the command on the left to the correct description on the right!\n" << endl; //Message to user

    correctPlace = shuffleDescriptions(commandsUsed[roundNumber - 1], descriptionOptions, numCommands); //Set up array containing possible description matches

    for (int i = 0; i < commandsUsed[roundNumber - 1]; i++) //Traverse to command to be presented to user
    {
        current = current->getNext();
    }

    cout << current->getCommand(); //Display command

    current = commandList.getHead(); //Reset current for description finding

    for (int i = 0; (i < 3); i++) //Loop through three descriptions
    {

        for (int j = 0; j < descriptionOptions[i]; j++) //Traverse list to description
        {
            current = current->getNext();
        }

        cout << "\t\t\t" << i + 1 << ". " << current->getDescription() << endl; //Print description

        current = commandList.getHead(); //Reset current to  find next description
    }

    for (int i = 0; i < commandsUsed[roundNumber - 1]; i++) //Traverse to used command to print correct answer
    {
        current = current->getNext();
    }

    do
    {

        cout << "Enter your guess: ";

        cin >> guess;

    } while ((guess < 1) || (guess > 3)); //Loops for invalid guess

    if (guess - 1 == correctPlace) //If guess is correct
    {
        cout << "Correct! " << current->getCommand() << " - " << current->getDescription() << " " << endl;

        player.score += 1; //Add to player score

        cout << "You have been awarded one point. Your point total is now " << player.score << endl;

        cout << "\n\n\n";

        cout << "Enter any key to continue..." << endl;

        cin >> throwaway; //Pause for user input

        cout << "\n\n\n" << endl; //Clear screen
    }
    else //Guess is incorrect
    {
        cout << "Incorrect! " << current->getCommand() << " - " << current->getDescription() << " " << endl;

        player.score -= 1; //Subtract from player score

        cout << "You have lost one point. Your point total is now " << player.score << endl;

        cout << "\n\n\n";

        cout << "Enter any key to continue..." << endl;

        cin >> throwaway; //Pause for user input

        cout << "\n\n\n" << endl; //Clear screen
    }

}



/************************************************************************
* Programming Assignment 1                                              *
* Play Full Game:                                                       *
*                                                                       *
* Description: Executes user selected number of rounds for the game.    *
************************************************************************/

void playFullGame(List<string, string> &commandList, Profile &player, int numCommands)
{
    int numQuestions; //Initialize number of questions that will be played during the game

    int commandsUsed[100]; //Initialize list to randomize order of commands

    string throwaway; //Initialize throwaway to pause system at select times

    cout << "Let's play!" << endl;

    do
    {
        if (numCommands < 30)
        {
            cout << "How many questions would you like to answer? (Enter a number between 5 and " << numCommands << "): ";
        }
        else
        {
            cout << "How many questions would you like to answer? (Enter a number between 5 and 30): ";
        }
        
        cin >> numQuestions;

        if ((numQuestions < 5) || (numQuestions > numCommands) || (numQuestions > 30))
        {
            cout << "That is not a valid number of questions.\n" << endl;
        }

    } while ((numQuestions < 5) || (numQuestions > numCommands) || (numQuestions > 30)); //Loop for invalid numbers of questions

    shuffleOrder(commandsUsed, numCommands); //Shuffle commands

    for (int i = 0; i < numQuestions; i++) //Loops for number of questions selected by user
    {
        playRound(commandList, player, i + 1, commandsUsed, numCommands);
    }

    cout << "And that's the game!" << endl;

    cout << "Nice job " << player.name << "! Your final score is: " << player.score << "\n\n" << endl;

    cout << "Enter any key to continue..." << endl;

    cin >> throwaway; //Pause until user input

    cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n" << endl; //Clear screen
}



/************************************************************************
* Programming Assignment 1                                              *
* Find Old Profile:                                                     *
*                                                                       *
* Description: Allows user to find a previously existing profile.       *
************************************************************************/

int findOldProfile(Profile profiles[100])
{
    string profileToFind; //Initialize profile for user input

    cout << "Enter the name of your profile: ";

    cin >> profileToFind;

    int i = 0;

    while ((profiles[i].name != "") && (profiles[i].name != profileToFind) && (i < 100)) //Loops until profile is found or end of list
    {
        i++;
    }

    if (profiles[i].name == profileToFind) //Profile found
    {
        cout << "Resuming game for " << profileToFind << endl;

        return i; //Return place of profile
    }
    else //Profile not found
    {
        cout << "No profile under that name exits.\n" << endl;
        
        return -1; //Return -1 for no profile found
    }
}



/************************************************************************
* Programming Assignment 1                                              *
* Add Command:                                                          *
*                                                                       *
* Description: Allows user to manually add a command for use in the     *
*              game.                                                    *
************************************************************************/

int addCommand(List<string, string> &commandList, int &numCommands)
{
    string newCommand, newDescription; //Initialize variables newCommand and newDescription for user input

    int duplicate = 0;

    ListNode<string, string>* current = commandList.getHead(); //Initialize node for traversal

    ListNode<string, string>* newNode = new ListNode<string, string>;

    if (commandList.getHead())
    {

        do
        {
            current = commandList.getHead(); //Initialize node for traversal

            cout << "Enter the new command or 'quit': ";

            cin >> newCommand;

            if ((newCommand == "quit") || (newCommand == "Quit")) //User selects quit
            {
                return numCommands; //Return to menu
            }

            while ((current->getNext()) && (current->getCommand() != newCommand)) //Traverse until duplicate command or end of 
            {
                current = current->getNext();
            }

            if (current->getCommand() == newCommand) //If command is duplicate
            {
                cout << "That command already exists." << endl;

                duplicate = 1;
            }
            else
            {
                duplicate = 0;
            }

        } while (duplicate); //Loop until new command is unique

        cout << "Enter the description of the new command in quotes: ";

        do
        {

            getline(cin, newDescription);

        } while (newDescription == "");

        newNode->setCommand(newCommand);

        newNode->setDescription(newDescription);

        commandList.insertAtFront(newNode); //Insert new node

        numCommands+= 1;
    }
    else if (!commandList.getHead()) //No list exists
    {
        cout << "Enter the new command or 'quit': ";

        cin >> newCommand;

        if ((newCommand == "quit") || (newCommand == "Quit")) //User selects quit
        {
            return numCommands; //Return to main menu
        }

        cout << "Enter the description of the new command in quotes: ";

        do
        {
            getline(cin, newDescription);

        } while (newDescription == "");

        newNode->setCommand(newCommand);

        newNode->setDescription(newDescription);

        commandList.setHead(newNode); //Set head to new node for empty list

        numCommands+= 1;
    }

    return numCommands;
}



/************************************************************************
* Programming Assignment 1                                              *
* Remove Command:                                                       *
*                                                                       *
* Description: Allows the user to select an existing command to remove  *
*              from game.                                               *
************************************************************************/

int removeCommand(List<string, string> &commandList, int &numCommands)
{
    string commandToRemove; //Initialize commandToRemove for user input

    int exists = 0; //Initialize variable used to determine if commandToRemove is in list

    ListNode<string, string>* current = commandList.getHead(); //Initialize node for traversal

    ListNode<string, string>* prev = current; //Initialize node for traversal

    do
    {
        current = commandList.getHead(); //Set current to head to traverse through list if user reselects commandToRemove

        cout << "Enter the command you want to remove or 'quit': ";

        cin >> commandToRemove;

        if ((commandToRemove == "quit") || (commandToRemove == "Quit")) //If user selects quit
        {
            return numCommands;
        }

        while ((current->getNext()) && (current->getCommand() != commandToRemove)) //Traverses until end of list or command is found
        {
            prev = current;

            current = current->getNext();
        }

        if (current->getCommand() != commandToRemove) //Command was not found in list
        {
            cout << "That command does not exist." << endl;

            exists = 0; //Command does not exist, set variable to loop
        }
        else
        {
            exists = 1; //Command exists
        }

    } while (!exists);

    commandList.removeCommand(commandToRemove); //Remove selected command

    numCommands -= 1;

    cout << "The command has been removed." << endl;

    return numCommands;
}



/************************************************************************
* Programming Assignment 1                                              *
* Read In Profiles:                                                     *
*                                                                       *
* Description: Read in already existing profiles from csv file.         *
************************************************************************/

void readInProfiles(ifstream &fileStream, Profile profiles[100])
{
    string newName; //Initialize name variable to read in from file

    int newScore; //Initialize score variable to read in from file

    int i, j; //Initialize variables to shift array as needed

    while (!fileStream.eof())
    {
        i = 0; //Set variables to zero after shift for next shift
        j = 1; //Set variables to zero after shift for next shift

        getline(fileStream, newName, ','); //Read in file until ','

        if ((newName == "") || (newName == "\n")) //If newName read in is empty
        {
            getline(fileStream, newName, ',');

            if ((newName == "") || (newName == "\n")) //If after reading in again, new name is still empty
            {
                break;
            }
        }

        fileStream >> newScore; //Read in integer score from file

        if (profiles[i].name == "") //If profile array is empty
        {
            profiles[i].name = newName;
            
            profiles[i].score = newScore;
        }
        else
        {
            while (profiles[j].name != "") //Find end of existing profiles
            {
                i++;

                j++;
            }

            for (i; i >= 0; i--, j--) //Loop until front of list
            {
                profiles[j].name = profiles[i].name; //Shift name forward one place in profile array

                profiles[j].score = profiles[i].score;
            }

            i = 0; //Reset i to start of array

            profiles[i].name = newName; //Set new name from file

            profiles[i].score = newScore; //Set new score from file
        }

        getline(fileStream, newName);
    }
}



/************************************************************************
* Programming Assignment 1                                              *
* Write Profiles:                                                       *
*                                                                       *
* Description: Writes profiles in profile array to csv file.            *
************************************************************************/

void writeProfiles(ofstream &fileStream, Profile profiles[100])
{
    int i = 0;

    while ((profiles[i].name != "") && (i < 100)) //Loop through profile list until empty
    {
        fileStream << profiles[i].name; //Write name to file

        fileStream << ","; //Separate name and score with ','

        fileStream << profiles[i].score; //Write score to file

        if (profiles[i + 1].name != "") //If not end of profile array
        {
            fileStream << "\n"; //Write newline to file
        }

        i++; //Increment to next profile in array
    }
}
