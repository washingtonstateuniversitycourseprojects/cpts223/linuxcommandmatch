/************************************************************************
* Programmer: Emma Mickas                                               *
* CptS 223, Spring 2020                                                 *
* Programming Assignment 1                                              *
* January 30, 2020                                                      *
* Decription: This file contains the list class template and all its    *
*             functions.                                                *
************************************************************************/

#pragma once
#include <iostream>
#include "ListNode.h"
#include <string>
#include <fstream>


template <class T1, class T2>
class List
{
private:

    ListNode<T1, T2>* head;

public:

    List();

    ~List();

    List(List<T1, T2>& newList);

    void destroyList(ListNode<T1, T2>* node);

    ListNode<T1, T2>* getHead();

    void setHead(ListNode<T1, T2>* newHead);

    void insertAtFront(ListNode<T1, T2>* newNode);

    void removeCommand(T1 commandRemove);

    int loadCommands(ifstream& fileStream, List &commandList);

    void writeCommands(ofstream& fileStream);
};





/************************************************************************
* Programming Assignment 1                                              *
* Constructor:                                                          *
*                                                                       *
* Description: Constructor for command list.                            *
************************************************************************/

template <class T1, class T2>
List<T1,T2>::List()
{
    head = nullptr;
}



/************************************************************************
* Programming Assignment 1                                              *
* Destructor:                                                           *
*                                                                       *
* Description: Destructor for list node.                                *
************************************************************************/

template <class T1, class T2>
List<T1, T2>::~List()
{
    destroyList(head);
}



/************************************************************************
* Programming Assignment 1                                              *
* Destroy List:                                                         *
*                                                                       *
* Description: Loops through all nodes to deallocate memory.            *
************************************************************************/

template <class T1, class T2>
void List<T1, T2>::destroyList(ListNode<T1, T2>* node)
{
    if (!node->getNext())
    {
        delete node;
    }
    else
    {
        destroyList(node->getNext());
    }
}


/************************************************************************
* Programming Assignment 1                                              *
* Copy Constructor:                                                     *
*                                                                       *
* Description: Copy constructor for list.                               *
************************************************************************/

template <class T1, class T2>
List<T1, T2>::List(List& newList)
{
    head = newList.getHead();
}



/************************************************************************
* Programming Assignment 1                                              *
* Get Head:                                                             *
*                                                                       *
* Description: Returns list head.                                       *
************************************************************************/

template <class T1, class T2>
ListNode<T1, T2>* List<T1, T2>::getHead()
{
    return head;
}



/************************************************************************
* Programming Assignment 1                                              *
* Set Head:                                                             *
*                                                                       *
* Description: Allows for changing list head.                           *
************************************************************************/

template <class T1, class T2>
void List<T1, T2>::setHead(ListNode<T1, T2>* newHead)
{
    head = newHead;
}



/************************************************************************
* Programming Assignment 1                                              *
* Insert At Front:                                                      *
*                                                                       *
* Description: Inserts new node at front of list.                       *
************************************************************************/

template <class T1, class T2>
void List<T1, T2>::insertAtFront(ListNode<T1, T2>* newNode)
{
    if (!head) //If list is empty
    {
        head = newNode; //Set head to new node
    }
    else
    {
        newNode->setNext(head); //Set new node next to head

        head = newNode; //Set head to new node
    }
}



/************************************************************************
* Programming Assignment 1                                              *
* Remove Command:                                                       *
*                                                                       *
* Description: Removes a command of a specific name.                    *
************************************************************************/

template <class T1, class T2>
void List<T1, T2>::removeCommand(T1 commandRemove)
{
    ListNode<T1, T2>* current = head;

    ListNode<T1, T2>* prev = current, * next = current->getNext();

    while (current->getCommand() != commandRemove) //Loop until command is found
    {
        prev = current; //Set previous to keep track of list

        current = next; //Increment current

        next = next->getNext();
    }

    prev->setNext(current->getNext()); //Set previous next to current next

    delete current; //Delete current
}



/************************************************************************
* Programming Assignment 1                                              *
* Load Commands:                                                        *
*                                                                       *
* Description: Reads in commands from a given file.                     *
************************************************************************/

template <class T1, class T2>
int List<T1, T2>::loadCommands(ifstream& fileStream, List &commandList)
{
    string newLine; //Initialize variable to store data from file

    int i = 0, numCommands = 0; //Initialize number of commands and variable to keep track of whether command or description is read in

    ListNode<T1, T2>* newNode; //Initialize new node for read ins

    while (!fileStream.eof()) //Loop until end of file
    {
        newNode = new ListNode<T1, T2>; //Create new node to store new command

        i = 0; //Reset i

        getline(fileStream, newLine, ','); //Read in file until ','

        while (i < 2) //Read in both command and description
        {

            if (newLine == "") //If new line is empty
            {
                break; //Break to get next command
            }
            else if (i == 0) //If command hasn't been read in yet
            {
                newNode->setCommand(newLine); //Set newNode command to read in command
            }
            else if (i == 1) //If description hasn't been read in yet
            {
                getline(fileStream, newLine); //Read in description

                newNode->setDescription(newLine); //Set newNode description to read in description

                numCommands++;
            }

            i++; //Increment i to reset command and description status
        }

        commandList.insertAtFront(newNode); //Insert new node with new command and description at front of command list

    }
    return numCommands;
}



/************************************************************************
* Programming Assignment 1                                              *
* Write Commands:                                                       *
*                                                                       *
* Description: Writes commands in list to csv file.                     *
************************************************************************/

template <class T1, class T2>
void List<T1, T2>::writeCommands(ofstream& fileStream)
{
    ListNode<T1, T2>* current = head; //Initialize current for traversing list

    while (current) //Loop until end of list
    {
        fileStream << current->getCommand(); //Write command to file

        fileStream << ","; //Separate command and description with comma

        fileStream << current->getDescription(); //Write description to file

        if (current->getNext()) //If this is not the end of the list
        {
            fileStream << "\n"; //Write newline to file to separate different commands
        }

        current = current->getNext(); //Increment current
    }
}

