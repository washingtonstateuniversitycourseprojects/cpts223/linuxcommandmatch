/************************************************************************
* Programmer: Emma Mickas                                               *
* CptS 223, Spring 2020                                                 *
* Programming Assignment 1                                              *
* January 30, 2020                                                      *
* Decription: This file contains the list node class template and all   *
*             its functions.                                            *
************************************************************************/

#pragma once
#include <iostream>
#include <string>
#include <fstream>
using namespace std;

template <class T1, class T2>
class ListNode
{
private:
    T1 command;
    T2 description;

    ListNode* nextNode;

public:

    ListNode();

    ~ListNode();

    ListNode(ListNode* newNode);

    T1 getCommand();

    T2 getDescription();

    ListNode* getNext();

    void setCommand(T1 newCommand);

    void setDescription(T2 newDescription);

    void setNext(ListNode* newNext);

};





/************************************************************************
* Programming Assignment 1                                              *
* Constructor:                                                          *
*                                                                       *
* Description: Constructor for the node in command list.                *
************************************************************************/

template <class T1, class T2>
ListNode<T1,T2>::ListNode()
{
    nextNode = nullptr;
}



/************************************************************************
* Programming Assignment 1                                              *
* Destructor:                                                           *
*                                                                       *
* Description: Destructor for list node.                                *
************************************************************************/

template <class T1, class T2>
ListNode<T1, T2>::~ListNode()
{
    nextNode = nullptr;
}



/************************************************************************
* Programming Assignment 1                                              *
* Copy constructor:                                                     *
*                                                                       *
* Description: Copy constructor for list node.                          *
************************************************************************/

template <class T1, class T2>
ListNode<T1, T2>::ListNode(ListNode* newNode)
{
    command = newNode->getCommand();

    description = newNode->getDescription();

    nextNode = newNode->getNext();
}



/************************************************************************
* Programming Assignment 1                                              *
* Get Command:                                                          *
*                                                                       *
* Description: Returns command from list node.                          *
************************************************************************/

template <class T1, class T2>
T1 ListNode<T1, T2>::getCommand()
{
    return command;
}



/************************************************************************
* Programming Assignment 1                                              *
* Get Description:                                                      *
*                                                                       *
* Description: Returns description from list node.                      *
************************************************************************/

template <class T1, class T2>
T2 ListNode<T1, T2>::getDescription()
{
    return description;
}



/************************************************************************
* Programming Assignment 1                                              *
* Set command:                                                          *
*                                                                       *
* Description: Allows for changing command in list node.                *
************************************************************************/

template <class T1, class T2>
void ListNode<T1, T2>::setCommand(T1 newCommand)
{
    command = newCommand;
}



/************************************************************************
* Programming Assignment 1                                              *
* Set Description:                                                      *
*                                                                       *
* Description: Allows for changing description in list node.            *
************************************************************************/

template <class T1, class T2>
void ListNode<T1, T2>::setDescription(T2 newDescription)
{
    description = newDescription;
}



/************************************************************************
* Programming Assignment 1                                              *
* Get next:                                                             *
*                                                                       *
* Description: Returns the next node of list node.                      *
************************************************************************/

template <class T1, class T2>
ListNode<T1, T2>* ListNode<T1, T2>::getNext()
{
    return nextNode;
}



/************************************************************************
* Programming Assignment 1                                              *
* Set Next:                                                             *
*                                                                       *
* Description: Sets the next node of list node.                         *
************************************************************************/

template <class T1, class T2>
void ListNode<T1, T2>::setNext(ListNode<T1, T2>* newNext)
{
    nextNode = newNext;
}

