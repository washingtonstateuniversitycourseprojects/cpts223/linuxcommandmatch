/************************************************************************
* Programmer: Emma Mickas                                               *
* CptS 223, Spring 2020                                                 *
* Programming Assignment 1                                              *
* January 30, 2020                                                      *
* Decription: This file contains function declarations to play match.   *
************************************************************************/

#pragma once
#include <iostream>
#include "List.h"
#include "profiles.h"

void displayRules();

int enterNewName(Profile profiles[]);

int numCommands(List<string, string> commandList);

int shuffleDescriptions(int correctDescription, int descriptionOptions[3], int numCommands);

void shuffleOrder(int commandsUsed[100], int numCommands);

void playRound(List<string, string> &commandList, Profile &player, int roundNumber, int commandsUsed[30], int numCommands);

void playFullGame(List<string, string> &commandList, Profile &player, int numCommands);

int findOldProfile(Profile profiles[100]);

int addCommand(List<string, string> &commandList, int &numCommands);

int removeCommand(List<string, string> &commandList, int &numCommands);

void readInProfiles(ifstream &fileStream, Profile profiles[100]);

void writeProfiles(ofstream &fileStream, Profile profiles[100]);
