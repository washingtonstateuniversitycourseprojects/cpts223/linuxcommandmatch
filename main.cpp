/************************************************************************
* Programmer: Emma Mickas                                               *
* CptS 223, Spring 2020                                                 *
* Programming Assignment 1                                              *
* January 30, 2020                                                      *
* Decription: This file contains the main function to execute program.  *
* ADVANTAGES/DISADVANTAGES LINKED LIST: One advantage to using a linked *
* list in this program is that adding or removing a command to/from the *
* list takes complexity O(1), no traversal is required. One disadvantage*
* to using a linked list is that each time you want to access a command *
* you must traverse the list node by node to find it if it is not in the*
* head node.                                                            *
* ADVANTAGES/DISADVANTAGES ARRAY: One advantage to using an array to    *
* store the profiles is that a particular profile can be acessed using  *
* an index immediately (O(1)). One disadvantage to using an array to    *
* store the profiles is that inserting an array requires looping through*
* all of the profiles existing in the array every time a new profile    *
* needs to be added to the front of the array.                          *
************************************************************************/

#include "playGame.h"
#include "profiles.h"



int main(int argc, char *argv[])
{
    int menu_choice = 0, playerNumber, i = 0; //Initialize menu choice and player number

    string throwaway; //Initialize variable for system pausing

    List<string, string> commandList; //Initialize command list

    Profile profiles[100]; //Initialize profile array

    while (i < 100) //Loop through profile array
    {
        profiles[i].score = 0; //Set initial profile scores to zero

        profiles[i].name = ""; //Set initial profile names to null

        i++; //Increment to traverse profile array
    }

    ifstream commandInFile, profileInFile; //Initialize in files

    ofstream commandOutFile, profileOutFile; //Initialize out files

    int numCommands = 0; //Initialize number of commands

    profileInFile.open("profiles.csv");

    readInProfiles(profileInFile, profiles);

    profileInFile.close();

    commandInFile.open("commands.csv"); //Open file containing commands

    numCommands = commandList.loadCommands(commandInFile, commandList);

    commandInFile.close();

    do
    {

        do
        {

            std::cout << "Main Menu:\n" << std::endl;

            std::cout << "1. Game Rules\n2. Play Game\n3. Load Previous Game\n4. Add Command\n5. Remove Command\n6. Exit\n" << std::endl;

            std::cin >> menu_choice;

        } while ((menu_choice < 1) || (menu_choice > 6)); //Loops until valid menu choice

        if (menu_choice == 1) //Display game rules
        {
            displayRules();

            cout << "Enter any key to continue..." << endl;

            cin >> throwaway; //Pause for user input

            cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n" << endl; //Clear screen
        }
        else if (menu_choice == 2) {

            playerNumber = enterNewName(profiles);

            playFullGame(commandList, profiles[playerNumber], numCommands);

            cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n" << endl; //Clear screen
        }
        else if (menu_choice == 3) {

            playerNumber = findOldProfile(profiles);

            if (playerNumber != -1) //If player exists
            {
                playFullGame(commandList, profiles[playerNumber], numCommands);
            }
            else
            {
                cout << "Enter any key to continue..." << endl;

                cin >> throwaway; //Pause for user input

                cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n" << endl; //Clear screen
            }
        }
        else if (menu_choice == 4) {

            numCommands = addCommand(commandList, numCommands);

            cout << "There are now " << numCommands << " commands." << endl;

            cout << "Enter any key to continue..." << endl;

            cin >> throwaway; //Pause for user input

            cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n" << endl; //Clear screen
        }
        else if (menu_choice == 5) {

            numCommands = removeCommand(commandList, numCommands);

            cout << "There are now " << numCommands << " commands." << endl;

            cout << "Enter any key to continue..." << endl;

            cin >> throwaway; //Pause for user input

            cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n" << endl; //Clear screen

        }
        else if (menu_choice == 6) {

            commandOutFile.open("commands.csv", std::ofstream::out | std::ofstream::trunc); //Open file to write commands

            commandList.writeCommands(commandOutFile); //Write commands to file

            commandOutFile.close(); //Close file after writing

            profileOutFile.open("profiles.csv"); //Open file to write profiles

            writeProfiles(profileOutFile, profiles); //Write profiles to file

            profileOutFile.close(); //Close file after writing

            break;

        }

    } while (menu_choice != 6);

    return 0;
}

