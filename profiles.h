/************************************************************************
* Programmer: Emma Mickas                                               *
* CptS 223, Spring 2020                                                 *
* Programming Assignment 1                                              *
* January 30, 2020                                                      *
* Decription: This file contains the profile struct used to populate    *
*             the profile array.                                        *
************************************************************************/

#pragma once
#include <iostream>
#include <string>
using namespace std;


struct Profile
{
    string name;

    int score;
};

